


module.exports = grammar( {
	name: 'Fox',

	word:                 $ => $.identifier,

	rules: {
		sourceFile:       $ => repeat( choice( $.comment, seq( optional( $.visibility ), choice( $.import, $.typeAliasDecl, $.classDecl, $.structDecl, $.interfaceDecl, $.funcDecl, $.varDecl ) ) ) ),

		// misc
		identifier:       $ => /([a-zA-Z_$][a-zA-Z_$0-9]*)/,
		dotIdentifier:    $ => seq( '.', $.identifier ),
		comment:          $ => choice( $.lineComment, $.docComment, $.blockComment ),
		genericUsageItem: $ => choice( $.type, $.literal ),
		genericUsage:     $ => seq( '<', $.genericUsageItem, repeat( seq( ',', $.genericUsageItem ) ), '>' ),
		scope:            $ => prec.left( seq( repeat1( seq( $.identifier, '::' ) ), optional( seq( $.identifier, $.genericUsage, '::' ) ) ) ),  // right-est element may be a template type
		type:             $ => prec.left( choice( seq( optional( seq( '&', optional( 'mut' ) ) ), optional( $.scope ), $.identifier, optional( $.genericUsage ), optional( seq( '[', ']' ) ) ), $.funcType ) ),
		funcType:         $ => seq( '(', optional( seq( $.type, repeat( seq( ',', $.type ) ) ) ), ')', '->', $.type ),
		import:           $ => seq( 'import', $.identifier, repeat( seq( ',', $.identifier ) ), 'from', $.identifier, repeat( seq( '.', $.identifier ) ) ),

		// declarations
		varDecl:          $ => seq( optional( 'mut' ), $.identifier, choice( seq( ':', $.type, optional( seq( '=', $.expr ) ) ), seq( '=', $.expr ) ) ),
		funcDecl:         $ => seq( optional( $.genericDecl ), $.identifier, $.parameterList, $.type, choice( $.block, seq( '=', $.expr ) ) ),
		parameterDecl:    $ => seq( $.identifier, ':', $.type, optional( seq( '=', $.expr ) ) ),
		parameterList:    $ => seq( '(', optional( seq( $.parameterDecl, repeat( seq( ',', $.parameterDecl ) ) ) ), ')' ),
		structuralItem:   $ => seq( $.identifier, '(', optional( seq( $.type, repeat( seq( ',', $.type ) ) ) ), ')', $.type ),
		structuralDecl:   $ => seq( '{', optional( seq( $.structuralItem, repeat( seq( ',', $.structuralItem ) ) ) ), '}' ),
		genericItem:      $ => choice( seq( optional( 'runtime' ), $.identifier, optional( seq( ':', choice( $.type, $.structuralDecl ) ) ) ), seq( 'const', $.identifier, ':', $.type, optional( seq( '=', $.expr ) ) ) ),
		genericDecl:      $ => seq( '<', optional( seq( $.genericItem, repeat( seq( ',', $.genericItem ) ) ) ), '>' ),
		typeAliasDecl:    $ => seq( 'typealias', $.identifier, optional( $.genericDecl ), '=', $.type ),
		superTypeDecl:    $ => seq( ':', $.type, repeat( seq( ',', $.type ) ), optional( ',' ) ),
		structDecl:       $ => seq( 'struct', $.identifier, optional( $.genericDecl ), optional( $.superTypeDecl ), '{', repeat( $.varDecl ), repeat( $.methodDecl ), '}' ),
		classDecl:        $ => seq( optional( 'abstract' ), 'class', $.identifier, optional( $.genericDecl ), optional( $.superTypeDecl ), '{', repeat( $.varDecl ), repeat( $.methodDecl ), '}' ),
		interfaceDecl:    $ => seq( 'interface', $.identifier, optional( $.genericDecl ), optional( $.superTypeDecl ), '{', repeat( $.methodDecl ), '}' ),
		methodDecl:       $ => seq( optional( 'override' ), $.funcDecl ),
		visibilityItem:   $ => seq( 'type', 'den', 'pkg', $.scope ),
		visibility:       $ => seq( 'pub', optional( seq( '(', optional( seq( $.visibilityItem, repeat( seq( ',', $.visibilityItem ) ), optional( ',' ) ) ), ')' ) ) ),

		// statements
		statement:        $ => choice( $.yield, $.return, $.break, $.continue, $.while, $.for, $.loop, $.varDecl, $.assign, $.expr ),
		return:           $ => seq( 'return', $.expr ),
		yield:            $ => seq( 'yield', $.expr ),
		break:            $ => seq( 'break' ),
		continue:         $ => seq( 'continue' ),
		while:            $ => seq( 'while', '(', $.expr, ')', $.statement ),
		for:              $ => seq( 'for', '(', $.identifier, 'in', $.expr, ')', $.statement ),
		loop:             $ => seq( 'loop', $.block ),
		assign:           $ => seq( optional( $.scope ), $.identifier, $.assignOp, $.expr ),

		// exprs
		expr:             $ => seq( $.casting ),
		casting:          $ => seq( $.checks, optional( seq( optional( 'reinterpret' ), 'as', $.type ) ) ),
		checks:           $ => seq( $.equality, optional( seq( choice( seq( 'is', $.type ), seq( 'in', $.expr ) ) ) ) ),
		equality:         $ => seq( $.comparison, repeat( seq( choice( '==', '!=' ), $.expr ) ) ),
		comparison:       $ => seq( $.term, repeat( seq( $.comparisonOp, $.expr ) ) ),
		term:             $ => seq( $.factor, repeat( seq( $.termOp, $.expr ) ) ),
		factor:           $ => prec.left( seq( $.subscript, repeat( seq( $.factorOp, $.expr ) ) ) ),
		subscript:        $ => seq( $.unary, optional( seq( '[', $.expr, ']' ) ) ),
		unary:            $ => prec( 2, seq( optional( $.unaryOp ), $.value ) ),
		value:            $ => choice( $.funcCall, $.varAccess, $.comptime, $.literal, $.grouping, $.lambda, $.if, $.when, $.block ),
		literal:          $ => choice( $.string, $.float, $.integer, 'true', 'false' ),
		grouping:         $ => seq( '(', $.expr, ')' ),
		comptime:         $ => seq( 'comptime', $.statement ),
		argItem:          $ => seq( optional( seq( $.identifier, ':' ) ), $.expr ),
		funcCall:         $ => seq( optional( $.scope ), $.identifier, optional( $.genericUsage ), '(', optional( seq( $.argItem, repeat( seq( ',', $.argItem ) ), optional( ',' ) ) ), ')' ),
		varAccess:        $ => seq( optional( $.scope ), $.identifier, repeat( $.dotIdentifier ) ),
		lambda:           $ => seq( '{', optional( seq( $.varDecl, optional( seq( ',', $.varDecl ) ) ) ), '->', repeat( choice( $.statement, $.comment ) ), '}' ),
		block:            $ => seq( '{', repeat( choice( $.statement, $.comment ) ), '}' ),
		if:               $ => seq( 'if', '(', $.expr, ')', $.statement, optional( seq( 'else', $.statement ) ) ),
		when:             $ => seq( 'when', '(', $.expr, ')', '{', repeat( $.whenEntry ), '}' ),
		whenEntry:        $ => seq( $.checks, '->', $.expr ),

		// operators
		factorOp:         $ => choice( '*', '/', '%', '**', '^', '<<', '>>', '&', '|' ),
		termOp:           $ => choice( '+', '-' ),
		unaryOp:          $ => choice( '+', '-', '~' ),
		comparisonOp:     $ => choice( '<', '<=', '>', '>=', '<=>', '&&', '||' ),
		logicOp:          $ => choice( '&&', '||', '^' ),
		assignOp:         $ => choice( '=', '+=', '-=', '*=', '/=', '|=', '&=', '^=', '%=', '**=', '<<=', '>>=' ),

		// other
		integer:          $ => /((0x[0-9A-Fa-f]+)|[1-9][0-9]*|(0[0-7]+)|0|'.')/,
		float:            $ => /([0-9]+\.([e|E][+|\-]?)?[0-9]+)/,
		string:           $ => /"([^"\\]|\\.)*"/,
		blockComment:     $ => /(\/\*(.|\n)*\*\/)/,
		docComment:       $ => /(\/\*\*(.|\n)*\*\/)/,
		lineComment:      $ => /(\/\/.*)/,
	}
} );
